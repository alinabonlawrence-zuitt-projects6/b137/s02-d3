package b137.alinabon.s02d3;

import java.util.HashMap;

public class HashMaps {

    public static void main(String[] args){
        System.out.println("HashMaps");


        // key-value pairs
        // we can access those items using indices

        // Syntax
        // HashMaps<key_data_type, value_data_type> <identifier> = new HashMap<key_data_type, value_data_type>();

        HashMap<String, String> jobPositions = new HashMap<String, String>();

        // add new elements
        jobPositions.put("Curry", "Point Guard");
        System.out.println(jobPositions);

        jobPositions.put("Thompson", "Shooting Guard");
        System.out.println(jobPositions);

        // Retrieve item via key
        System.out.println(jobPositions.get("Curry"));

        System.out.println(jobPositions.keySet());
        System.out.println(jobPositions.values());

        // removing an existing item
        jobPositions.remove("Curry");
        System.out.println(jobPositions);
    }
}
